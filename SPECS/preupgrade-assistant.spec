%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

# build_ui macro can be overriden by using the rpmbuild --define option
%if 0%{?rhel}
%{!?build_ui: %global build_ui 1}
%else
%{!?build_ui: %global build_ui 0}
%endif # RHEL

%if %{build_ui}
%global         django_version  1.5.5
%global         south_version   0.8.4
%endif # build_ui

Name:           preupgrade-assistant
Version:        2.6.2
Release:        1%{?dist}
Summary:        Preupgrade Assistant advises on feasibility of system upgrade or migration
Group:          System Environment/Libraries
License:        GPLv3+

Source0:        %{name}-%{version}.tar.gz
%if %{build_ui}
Source1:        Django-%{django_version}.tar.gz
Source2:        south-%{south_version}.tar.gz
%endif # build_ui

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  rpm-devel
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  rpm-python
BuildRequires:  diffstat
%if 0%{?rhel}
BuildRequires:  pykickstart
%endif # RHEL
Requires(post):   /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Requires:       coreutils grep gawk
Requires:       sed findutils bash
Requires:       rpm-python
Requires:       redhat-release
Requires:       yum-utils
Requires:       openscap%{?_isa} >= 0:1.0.8
Requires:       openscap-engine-sce%{?_isa} >= 0:1.0.8
Requires:       openscap-utils%{?_isa} >= 0:1.0.8
Requires:       pykickstart
Conflicts:      %{name}-tools < 2.1.0-1
Obsoletes:      %{name} < 2.1.3-1

%description
The Preupgrade Assistant is a framework designed to run the Preupgrade
Assistant modules, which analyze the system for possible in-place
upgrade limitations. It is based on a modular system, with each module
performing a separate test, checking for package removals, incompatible
obsolete packages, changes in libraries, users, groups, services, or
incompatibilities of command-line options or configuration files. It is
able to execute post-upgrade scripts to finalize complex tasks after
the system upgrade. Apart from performing the in-place upgrades, the
Preupgrade Assistant is also capable of migrating the system. It then
produces a report, which assists you in performing the upgrade itself
by outlining potential problem areas and by offering suggestions about
mitigating any possible incompatibilities. The Preupgrade Assistant
utility is a Red Hat Upgrade Tool prerequisite for completing a successful
in-place upgrade to the next major version of Red Hat Enterprise Linux.

%if %{build_ui}
%package ui
Summary:    Preupgrade Assistant Web Based User Interface
Group:      System Environment/Libraries
Requires:   %{name}
Requires:   sqlite
Requires:   mod_wsgi
Requires:   httpd
Requires:   %{name} = %{version}-%{release}

%description ui
Graphical interface for Preupgrade Assistant. This can be used
for inspecting results.
%endif # build_ui

%package tools
Summary:    Preupgrade Assistant tools for generating modules
Group:      System Environment/Libraries
Provides:   preupg-xccdf-compose = %{version}-%{release}
Requires:   %{name} = %{version}-%{release}
Obsoletes:  %{name}-tools < 2.1.3-1
%description tools
Tools for building/generating modules used by Preupgrade Assistant.
User can specify only INI file and scripts and other stuff needed by
OpenSCAP is generated automatically.

%prep
%setup -n %{name}-%{version} -q

%if %{build_ui}
# Unpack UI-related tarballs
%setup -q -n %{name}-%{version} -D -T -a 1
%setup -q -n %{name}-%{version} -D -T -a 2
%endif # build_ui

%build
%{__python} setup.py build

%if %{build_ui}
pushd Django-%{django_version}
%{__python} setup.py build
popd
pushd South-%{south_version}
%{__python} setup.py build
popd
%else
# Prevent from compiling UI
rm -rf build/lib/preupg/ui
%endif # build_ui


%check
# Switch off tests until issue with finding /etc/preupgrade-assistant.conf
# is resolved
#%%{__python} setup.py test

%install
%{__python} setup.py install --skip-build --root=$RPM_BUILD_ROOT

install -d -m 755 $RPM_BUILD_ROOT%{_localstatedir}/log/preupgrade
install -d -m 755 $RPM_BUILD_ROOT%{_mandir}/man1
install -p man/preupg.1 $RPM_BUILD_ROOT%{_mandir}/man1/
install -p man/preupgrade-assistant-api.1 $RPM_BUILD_ROOT%{_mandir}/man1/
install -p man/preupg-content-creator.1 $RPM_BUILD_ROOT%{_mandir}/man1/
install -p man/preupg-diff.1 $RPM_BUILD_ROOT%{_mandir}/man1/

%if %{build_ui}
######### UI packaging #######################################
mkdir -m 644 -p ${RPM_BUILD_ROOT}%{_sharedstatedir}/preupgrade/{results,upload,static}
touch ${RPM_BUILD_ROOT}%{_sharedstatedir}/preupgrade/{db.sqlite,secret_key}

sed -r \
  -e "s;^DATA_DIR = .*$;DATA_DIR = '%{_sharedstatedir}/preupgrade';" \
  -i ${RPM_BUILD_ROOT}%{python_sitelib}/preupg/ui/settings.py

sed \
    -e 's;WSGI_PATH;%{python_sitelib}/preupg/ui/wsgi.py;g' \
    -e 's;STATIC_PATH;%{_sharedstatedir}/preupgrade/static;g' \
    -i ${RPM_BUILD_ROOT}%{_sysconfdir}/httpd/conf.d/99-preup-httpd.conf.{private,public}

# install django
pushd Django-%{django_version}
%{__python} setup.py install --skip-build --root ${RPM_BUILD_ROOT}
popd
pushd South-%{south_version}
%{__python} setup.py install --skip-build --root ${RPM_BUILD_ROOT}
popd

# remove .po files
find    ${RPM_BUILD_ROOT}%{python_sitelib}/django -name "*.po" | xargs rm -f

# remove bin/django-admin and *.egg-info
rm -rf  ${RPM_BUILD_ROOT}%{_bindir}/django-admin* \
        ${RPM_BUILD_ROOT}%{python_sitelib}/Django-%{django_version}-py*.egg-info \
        ${RPM_BUILD_ROOT}%{python_sitelib}/South-%{south_version}-py*.egg-info

# move django and south to preupg/ui/lib
mkdir   ${RPM_BUILD_ROOT}%{python_sitelib}/preupg/ui/lib
mv      ${RPM_BUILD_ROOT}%{python_sitelib}/{django,south} \
        ${RPM_BUILD_ROOT}%{python_sitelib}/preupg/ui/lib/

######### END UI packaging ################################
%else # do not build UI
# remove UI-related files
rm -rf  ${RPM_BUILD_ROOT}%{python_sitelib}/preupg/ui/
rm -f   ${RPM_BUILD_ROOT}%{_bindir}/preupg-ui-manage
rm -f   ${RPM_BUILD_ROOT}%{_sysconfdir}/httpd/conf.d/99-preup-httpd.conf.*
rm -f   ${RPM_BUILD_ROOT}%{_docdir}/%{name}/README.ui
%endif # build_ui

######### FILELISTS #######################################
# generate file lists for cleaner files section
get_file_list() {
    find ${RPM_BUILD_ROOT} -type $1 | grep -o $2 \
        | grep -vE "$3" | sed "$4" >> $5
}
### preupgrade-assistant ###
get_file_list f %{python_sitelib}/.*$  "preupg/(ui|creator)|\.pyc$" \
    "s/\.py$/\.py\*/" preupg-filelist
get_file_list d %{python_sitelib}/.*$ "preupg/(ui|creator)|\.pyc$" \
    "s/^/\%dir /" preupg-filelist
%if %{build_ui}
### preupgrade-assistant-ui ###
get_file_list f %{python_sitelib}/preupg/ui.*$ "/ui/settings.py|\.pyc$" \
    "s/\.py$/\.py\*/" preupg-ui-filelist
get_file_list d %{python_sitelib}/preupg/ui.*$ " " \
    "s/^/\%dir /" preupg-ui-filelist
%endif # build_ui
######### END FILELISTS ###################################

%if ! 0%{?fedora:1}
# clean section should not be used on Fedora per Guidelines
%clean
rm -rf $RPM_BUILD_ROOT
%endif not Fedora

%post
/sbin/ldconfig

######### UI (UN)INSTALLATION scriplets ###################
%if %{build_ui}
%post ui
# populate DB and/or apply DB migrations
su apache - -s /bin/bash -c "preupg-ui-manage syncdb --migrate --noinput" >/dev/null || :
# collect static files
su apache - -s /bin/bash -c "preupg-ui-manage collectstatic --noinput" >/dev/null || :
if [ "$1" == 1 ]; then
    # allow httpd to run preupgrade ui
    setsebool httpd_run_preupgrade on
fi
# restart apache
service httpd condrestart

%postun ui
# $1 holds the number of preupgrade-assistant-ui
# packages which will be left on the system when
# the uninstallation completes.
if [ "$1" == 0 ]; then
    # disallow httpd to run preupgrade ui
    setsebool httpd_run_preupgrade off
    # restart apache
    service httpd condrestart
fi
%endif # build_ui
######### END UI (UN)INSTALLATION scriplets ###############


%postun -p /sbin/ldconfig

%files -f preupg-filelist
%defattr(-,root,root,-)
%attr(0755,root,root) %{_bindir}/preupg
%attr(0755,root,root) %{_bindir}/preupg-kickstart-generator
%dir %{_localstatedir}/log/preupgrade
%dir %{_datadir}/preupgrade/
%dir %{_docdir}/%{name}
%config(noreplace) %{_sysconfdir}/preupgrade-assistant.conf
%{_sysconfdir}/bash_completion.d/preupg.bash
%{_datadir}/preupgrade/data
%{_datadir}/preupgrade/common.sh
%doc %{_docdir}/%{name}/README
%doc %{_docdir}/%{name}/README.kickstart
%{!?_licensedir:%global license %%doc}
%license %{_docdir}/%{name}/LICENSE
%attr(0644,root,root) %{_mandir}/man1/preupg.*

%if %{build_ui}
%files ui -f preupg-ui-filelist
%defattr(-,root,root,-)
%attr(0755,root,root) %{_bindir}/preupg-ui-manage
%verify(not md5 size mtime) %config %{python_sitelib}/preupg/ui/settings.py
%{python_sitelib}/preupg/ui/settings.py[c|o]
%config(noreplace) %{_sysconfdir}/httpd/conf.d/99-preup-httpd.conf.*
%attr(0744, apache, apache) %dir %{_sharedstatedir}/preupgrade/
%ghost %config(noreplace) %{_sharedstatedir}/preupgrade/db.sqlite
%ghost %config(noreplace) %{_sharedstatedir}/preupgrade/secret_key
%doc %{_docdir}/%{name}/README.ui
%endif # build_ui

%files tools
%defattr(-,root,root,-)
%attr(0755,root,root) %{_bindir}/preupg-xccdf-compose
%attr(0755,root,root) %{_bindir}/preupg-content-creator
%attr(0755,root,root) %{_bindir}/preupg-diff
%{python_sitelib}/preupg/creator/
%doc %{_docdir}/%{name}/module_writing_tutorial
%attr(0644,root,root) %{_mandir}/man1/preupgrade-assistant-api.*
%attr(0644,root,root) %{_mandir}/man1/preupg-content-creator.*
%attr(0644,root,root) %{_mandir}/man1/preupg-diff.*

%changelog
* Tue Jul 21 2020 Petr Stodulka <pstodulk@redhat.com> - 2.6.2-1
- Rebased to upstream version 2.6.2
- enable the httpd_run_preupgrade SELinux boolean permanently for Web UI
- Fix Web UI overall report risk level
  Resolves: rhbz#1707913
- Check that installed openscap rpms are related to the system arch
  Resolves: rhbz#1804691
- Resolves: rhbz#1859309

* Wed Sep 05 2018 Petr Stodulka <pstodulk@redhat.com> - 2.6.1-1
- Rebased to upstream version 2.6.1
- fixes issues with redirected stdin
- provides paths to all significant directories in PA API
  Resolves: rhbz#1613345

* Fri Mar 23 2018 Michal Bocek <mbocek@redhat.com> - 2.6.0-1
- Rebased to upstream version 2.6.0
- Support for RHEL 6.6
- [home-dirs] section removed from the /etc/preupgrade-assistant.conf
  -> it won't be replaced during installation of this new version of the tool
     and that could cause the tool to fail during its execution

* Wed Feb 21 2018 Michal Bocek <mbocek@redhat.com> - 2.5.2-1
- Rebased to upstream version 2.5.2
- Fixed running 3rd party modules. How to create one:
  https://access.redhat.com/articles/1224543
- Fixed error when a file on the analyzed system has a name with non-UTF-8 chars
- In the executable.log file, include only non-symlinked executable files present
  only on a local file system (the file is provided to modules through API as
  VALUE_EXECUTABLES)

* Wed Nov 08 2017 Michal Bocek <mbocek@redhat.com> - 2.5.1-1
- Rebased to upstream version 2.5.1
- Fixed --upload, --riskcheck and --cleanup options when no module set is
  present in /usr/share/preupgrade
  Resolves: rhbz#1372090
- Not inserting 'solution' key to module.ini by preupg-content-creator
  Related: rhbz#1402480
- Fixed --list-rules with custom module set
  Resolves: rhbz#1362708
- Added httpd to the dependencies for the preupgrade-assistant-ui package
- Fixed replacing placeholders with solution texts during the XML/HTML report
  generation

* Tue Oct 17 2017 Michal Bocek <mbocek@redhat.com> - 2.5.0-1
- rebased to upstream version 2.5.0
New:
- Name of a module set directory can now be arbitrary and 'properties.ini' file
  is now required in the directory
- Module set can now contain an executable 'init' script which is executed
  after the "common" files-gathering stage and before execution of the modules
- Possible to skip building UI now
Changed:
- READMEs moved from '/usr/share/preupgrade' to '/usr/share/doc'
- Removed CPE platform from the result report
- Removed obsolete 'RESULT_FAILED' and 'check_root()' from bash API and
  'exit_failed()' from python API
- Removed deprecated 'preupg-create-group-xml' utility - it's superseded by the
  'preupg-content-creator' utility
- It's not allowed anymore to have 'solution' and 'check_script' keys in
  'module.ini' - the solution file and check script need to named
  'solution.txt' and 'check'
  Related: rhbz#1400680
Fixed:
- '<module set directory>/common/release_version' file and
  '<module set directory>/common/<src_arch>-<dst_arch>' directory are now not
  required for performing the system assessment
- Before the STDOUT and STDERR was not shown for the 'pass' and 'fixed' results
  in the simple style report ('--old-report-style' option). Now they are shown
  no matter what the result of a module is.
- 'preupg-content-creator': Not entering module's 'description' or 'title'
  could break processing of a module set. They are mandatory to input now.
- Module set 'all-xccdf.xml' is not being corrupted now in case of using the
  '--contents' option
- Fixed links to files and directories in Web UI reports
- The Preupgrade Assistant can run again on RHEL 6.7. The previous release
  could run on RHEL 6.8+.

* Mon Jul 24 2017 Michal Bocek <mbocek@redhat.com> - 2.4.4-1
- Return code 28 instead of 0-2 when upload fails
  Resolves: rhbz#1391968

* Mon Jul 17 2017 Michal Bocek <mbocek@redhat.com> - 2.4.3-1
- Fixed use of the -u option without the -r option
  Resolves: rhbz#1391968

* Wed Jul 12 2017 Michal Bocek <mbocek@redhat.com> - 2.4.2-1
- Fixed losing ownership when copying 'clean' configs after the system upgrade
  Resolves: rhbz#1469168

* Fri Jun 16 2017 Michal Bocek <mbocek@redhat.com> - 2.4.1-1
- Fixed XML parsing error - caused crash during system assessment when custom
  modules compiled with preupg-xccdf-compose were used
- Fixed UI to show report creation time, not upload time
  Resolves: rhbz#1244881
- Fix --list-rules option when multiple module sets were available
  Resolves: rhbz#1418509
- Improved check whether Preupgrade Assistant modules INI file contains all
  the required items
- Improved error message when non-existing module set name was used
  Resolves: rhbz#1332775
- Added printing of each module exec time during the assessment
  Resolves: rhbz#1392026

* Wed May 03 2017 Michal Bocek <mbocek@redhat.com> - 2.4.0-1
- removed result-user/admin.xml/html report creation
  Resolves rhbz#1414322
- removed /usr/bin/premigrate executable
- fixed XML parse errors when generating Preupgrade Assistant modules
  using preupg-xccdf-compose
- now possible to override default data directory
  (/usr/share/preupgrade/data)

* Tue Mar 07 2017 Michal Bocek <mbocek@redhat.com> - 2.3.3-2
- update preupgrade-assistant-scripts.patch per upstream -
  forgotten to do so during rebase to version 2.3.2

* Tue Mar 07 2017 Michal Bocek <mbocek@redhat.com> - 2.3.3-1
- rebased to upstream version 2.3.3
- preupg-xccdf-compose now generates valid XML for OpenSCAP - html
  namespace was missing

* Fri Mar 03 2017 Michal Bocek <mbocek@redhat.com> - 2.3.2-2
- documentation text polishing

* Fri Mar 03 2017 Michal Bocek <mbocek@redhat.com> - 2.3.2-1
- rebased to upstream version 2.3.2
- added preupg-diff tool
  Resolves: rhbz#1418697
- add check for required data for crossarch migration
  Resolves: rhbz#1414388
- improve handling of errors/exceptions
- in kickstart use 'autopart' instead of custom partioning
  Related: rhbz#1414408, 1414341, 1414344
- do not expose root password in generated kickstart
- clean all temp files from /tmp/
  Resolves: rhbz#1417979
- update man and help page
  Related: rhbz#1414369, 1414422, 1414407, 1414392, 1414339
- fix running postmigrate scripts
- keep selinux contexts of the migrated files
- after migration backup the clean system configs with
  .preupg_save suffix instead of .rpmsave
- add two new return codes: 29 for internal error and
  30 for user abort
- return code 22 meaning changed to invalid CLI option
- handle errors when result XML contains BEL character
- drop check of empty postmigrate.d dir during generating kickstart
- do not scare users by warning that they should backup the system
  before the assessment
  Resolves: rhbz#1414329

* Thu Jan 19 2017 Michal Bocek <mbocek@redhat.com> - 2.3.0-1
- updated spec file to have it the same for both RHEL 5 and RHEL 6
  Resolves: rhbz #1415304
- log_* Bash API functions now accept one parameter instead of multiple
  Resolves: rhbz #1415296
- removed [component] part from log messages that appear in result.xml/html
  in stdout of modules. With this, API function set_component was removed.
  Resolves: rhbz #1372871
- updated man page to reflect current return codes of Preupgrade Assistant
  Resolves: rhbz #1404740
- fixed import in preupg-create-group-xml tool
  Resolves: rhbz #1402210
- added package DSA signature to the generated list of installed packages
- fixed a traceback when generating kickstart
- fixed typo causing some packages not to be added to kickstart
  Related: rhbz #1414373

* Wed Nov 30 2016 Michal Bocek <mbocek@redhat.com> - 2.2.0-1
- removed unused directory /etc/preupgrade-assistant.d/ and /usr/share/premigrate/
- added postmigrate.d folder - user defined scripts placed to this folder are
  executed after the migration
- two types of HTML report are supported - simple (aka old) and complex - both
  compatible with openscap version 1.2.12
- cleaned up hierarchy/directory structure, most notably python module
  renamed from preup to preupg
- removed "unknown" shell API exit value
- renamed binary preup_ui_manage to preupg-ui-manage
- preupg-content-creator - added default script type (shell) when creating new module
- fixed parsing hooks to be used by modules
- Resolves: rhbz #1392901, #1393080

* Mon Oct 10 2016 Petr Hracek <phracek@redhat.com> - 2.1.10-6
- default value in script type is not picked on Enter
- Related: #1332792

* Wed Oct 05 2016 Petr Hracek <phracek@redhat.com> - 2.1.10-5
- Fix binary_req is ignored
- Resolves: #1380139

* Tue Oct 04 2016 Petr Hracek <phracek@redhat.com> - 2.1.10-4
- Fix deploy_hook to proper directory
- Resolves: #1381359

* Tue Sep 27 2016 Petr Hracek <phracek@redhat.com> - 2.1.10-3
- Fix several deploy_hook bugs
- Resolves: #1378191, #1378205, #1378215, #1378219

* Wed Sep 21 2016 Petr Hracek <phracek@redhat.com> - 2.1.10-2
- Fix regression in debug messages
- Resolves: #1371701

* Wed Sep 21 2016 Petr Hracek <phracek@redhat.com> - 2.1.10-1
- Support full path in deploy_hook function
- Related #1334903

* Mon Sep 19 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-9
- Fix dist-mode is ignored
- get_dist_native_list is broken
- Resolves: #1376640
- Resolves: #1375813

* Thu Sep 15 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-8
- Fix deploy_hook function. Returns exit_error statuses
- Related #1334903

* Wed Sep 14 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-7
- Fix MODULE_PATH is empty or not defined
- Resolves #1334903

* Wed Sep 14 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-6
- Fix documentation
- Resolves: #1272917

* Wed Sep 07 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-5
- Revert return values to previous state.
- Resolves: #1373493

* Mon Sep 05 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-4
- Remove print in check_inplace_risk function
- Resolves: #1372870

* Thu Sep 01 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-3
- preupg return code: Fix fail without risk is error
- Resolves: #1362659

* Wed Aug 31 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-2
- Update dependencies to the newest openscap-1.2.8
- Resolves #1371832

* Thu Aug 25 2016 Petr Hracek <phracek@redhat.com> - 2.1.9-1
- Fix return values
- Resolves #1362659

* Wed Aug 24 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-7
- Fix deploy_hook function
- Related #1334903

* Mon Aug 22 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-6
- Fix order of exist values

* Mon Aug 22 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-5
- Missing files in tarball
- Resolves #1352171
- Related #1361715

* Wed Aug 17 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-4
- Fix NONE risk. Another patch
- Related #1331629

* Tue Aug 16 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-3
- Fix return values
- Removing all sys.exit calling
- Resolves #1361715

* Fri Jul 29 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-2
- Fixes build modules and prints output in case of wrong assessment
- Resolves #1361489

* Tue Jul 26 2016 Petr Hracek <phracek@redhat.com> - 2.1.8-1
- Several documentation updates (#1272917)
- Updated tests for kickstart generation

* Wed Jul 13 2016 Petr Hracek <phracek@redhat.com> - 2.1.7.post21-1
- Fix issues caused by prompt

* Fri Jul 01 2016 Petr Hracek <phracek@redhat.com> - 2.1.7.post18-1
- This version fixes several bugs
- Resolves #1309536, #1330883, #1334903
- Resolves #1332777, #1332792, #1325393
- Resolves #1331629

* Tue Jun 07 2016 Petr Hracek <phracek@redhat.com> - 2.1.6.post41-6
- Move preupgrade.ks file to kickstart directory
  Resolves: #1318602

* Fri Apr 29 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.6-5
- fix parsing of configuration file
  Resolves: #1256685

* Thu Apr 28 2016 Petr Hracek <phracek@redhat.com> - 2.1.6-4
- Modify kickstart generation based on preupgrade-assistant-el6toel7 data

* Tue Apr 26 2016 Petr Hracek <phracek@redhat.com> - 2.1.6-3
- Fix traceback preupg-content-creator
- Fix problem with generating assessment for specific mode
  Resolves: #1317124
  Resolves: #1330716

* Fri Apr 22 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.6-2
- remove old sources and patch
- resolves xsl pain - patch was removed previously, so added back
  modified patch, suitable to current version of PA: 2.1.6
  Resolves: #1304772 #1312702


* Wed Apr 20 2016 Petr Hracek <phracek@redhat.com> - 2.1.6-1
- Fix package kickstart generation
  Resolves: #1261291

* Mon Apr 11 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.5-5
- fix API for python's scripts - check of installed packages
  Resolves: #1318431

* Wed Mar 16 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.5-4
- fix wrong condition in check_applies_to which evaluates if
  script is applicable or not; negation of the condition
  Resolves: #1318431

* Tue Mar 08 2016 Petr Hracek <phracek@redhat.com> - 2.1.5-3
- Fix for preupg-content-creator. Introduce manual pages
  Resolves: #1253682

* Wed Mar 02 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.5-2
- solve xsl pain for now, modified preup.xsl
  - correction of paths and remove section which implies dump info
    in html with new OpenSCAP
  - added xsl files from openscap 1.0.8-1 to our xsl directory
  - reverted part of previous fix
  Resolves: #1304772 #1312702

* Fri Feb 26 2016 Petr Hracek <phracek@redhat.com> - 2.1.5-1
- New upstream release with several bugs
- Kickstart bugs
- API are the same
- UI fixes
  Resolves: #1310056, #1293410, #1302267, #1302278, #1302303,
  Resolves: #1302309, #1310056

* Thu Feb 18 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.4-10
- fix syntax error in previous patch
  Resolves: #1309519

* Mon Feb 15 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.4-9
- fix common.sh - use local variable instead of global

* Mon Feb 15 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.4-8
- Fix function get_dist_native_list in common.sh
  Resolves: #1256685

* Mon Feb 08 2016 Petr Hracek <phracek@redhat.com> - 2.1.4-7
- Fix for report generation is broken
- Resolves: #1304772

* Fri Jan 22 2016 Petr Hracek <phracek@redhat.com> - 2.1.4-6
- Staticdata are part of content path

* Fri Jan 08 2016 Petr Hracek <phracek@redhat.com> - 2.1.4-5
- Devel mode has own section in configuration file

* Thu Jan 07 2016 Jakub Dorňák <jdornak@redhat.com> - 2.1.4-4
- Fixed expanding and collapsing of results in UI
  Resolves: #1231400

* Thu Jan 07 2016 Petr Stodulka <pstodulk@redhat.com> - 2.1.4-3
- fix changes due to preupgrade-assistant-staticdata (keep common directory
  with its content)
- add function get_dist_native_list to API
- modify is_dist_native
    - return True/False in python
    - use log_warning instead of log_info when package is removed
  Resolves: #1256685, #1296298

* Wed Jan 06 2016 Jakub Dorňák <jdornak@redhat.com> - 2.1.4-2
- Add README.ui
  Resolves: #1225844

* Wed Jan 06 2016 Petr Hracek <phracek@redhat.com> - 2.1.4-1
- New upstream release 2.1.4

* Tue Dec 22 2015 Petr Hracek <phracek@redhat.com> - 2.1.3-3
- Fix typo in preupgrade-assistant-devel.patch

* Wed Dec 09 2015 Petr Hracek <phracek@redhat.com> - 2.1.3-2
- Add patch for DEVEL_MODE

* Wed Dec 09 2015 Petr Hracek <phracek@redhat.com> - 2.1.3-1
- New upstream release 2.1.3

* Tue Oct 27 2015 Petr Hracek <phracek@redhat.com> - 2.1.1-5
- Rebuild for fast errata refresh.

* Mon Oct 26 2015 Petr Hracek <phracek@redhat.com> - 2.1.1-4
- Another rebuild because of QA testing.

* Mon Oct 26 2015 Petr Hracek <phracek@redhat.com> - 2.1.1-3
- Rebuild because of QA testing.

* Mon Sep 14 2015 Petr Hracek <phracek@redhat.com> - 2.1.1-2
- Update changelog
- New upstream release 2.1.1
- kickstart fixes

* Wed Sep 09 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-7
- Include /tmp/part-include into kickstart file
  Resolves (#1252916)

* Tue Sep 08 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-6
- Use RHRHEL7rpmlist file instead of RHRHEL7rpmlist_kept

* Tue Sep 08 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-5
- fix common.sh script

* Tue Sep 08 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-4
- Add patch for common.sh

* Mon Sep 07 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-3
- Info what to do with pre-generated kickstart
  Resolves (#1253680)

* Mon Sep 07 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-2
- API with path to static lists
- symbolic link to complete set of variants
- preupg --kickstart informs to run preupg
  Resolves (#1247921, #1254586, #1260008)

* Thu Aug 13 2015 Petr Hracek <phracek@redhat.com> - 2.1.0-1
- New upstream version 2.1.0
  Resolves #1215685
  Resolves #1229790

* Wed Jul 29 2015 Petr Hracek <phracek@redhat.com> - 2.0.3-12
- RFE Provide a configuration file override implementation
  for customized symbolic links
  Resolves #1234557

* Thu Jun 18 2015 Petr Hracek <phracek@redhat.com> - 2.0.3-11
- Fix for placeholder text instead of Solution
  Resolves #1232961

* Wed Jun 17 2015 Petr Stodulka <pstodulk@redhat.com> - 2.0.3-10
- recover patch from 2.0.3-7 - original rhbz #1225758
  Resolves #1232863

* Wed Jun 17 2015 Petr Hracek <phracek@redhat.com> - 2.0.3-9
- Placeholder text instead of Solution
  Resolves #1229810
- Incomplete items in result-admin.html
  Resolves #1229877

* Thu May 28 2015 Jakub Dorňák <jdornak@redhat.com> - 2.0.3-8
- Apply UI changes
  Resolves #1196166

* Thu May 28 2015 Jakub Dorňák <jdornak@redhat.com> - 2.0.3-7
- fix call of get_file_content
  Resolves #1225758

* Wed May 20 2015 Petr Stodulk <pstodulk@redhat.com> - 2.0.3-3
- Correction of *six patch - stdout must be init by 'str()' as binary type
  otherwise we can get ascii error due to previous performance fix
  Resolves #1222935

* Wed May 20 2015 Petr Hracek <phracek@redhat.com> - 2.0.3-2
- Test if python-six exists in python sources
  Resolves #1222935

* Tue May 19 2015 Petr Hracek <phracek@redhat.com> -2.0.3-1
- Fix for admin and user reports
  Related: #1212810

* Wed May 13 2015 Petr Stodulka <pstodulk@redhat.com> - 2.0.2-1
- PA was rebased in some previous commit, so bump version
- Fix performance problems
  Resolves: #1221138

* Thu May 07 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-16
- Add pykickstart to Requires
- Fix RepoData.patch

* Wed Apr 29 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-15
Resolves: #1215965

* Mon Apr 13 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-14
- Add missing patch for RepoData

* Fri Apr 10 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-13
- Resolves #1209782 Preupgrade freezes and never ends in i686 arch

* Tue Mar 17 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-12
- Mode option doesn't process any contents (#1203141)
- Add support migrate and upgrade to check scripts (#1200033)

* Tue Mar 17 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-11
- Add support migrate and upgrade to check scripts (#1200033)

* Tue Mar 17 2015 Petr Stodulka <pstodulk@redhat.com> - 2.0.1-10
- Added requires: redhat-release
  Solve issue with missing /etc/redhat-release, which is for us
  best solution for detection of system variant.

* Mon Mar 16 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-9
- Fix problem with decoding

* Wed Mar 11 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-8
- Wrong dependencies in preupgrade-assistant

* Tue Mar 10 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-7
- Multilib problems add missing BuildRequires

* Tue Mar 10 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-6
- Multilib problems

* Mon Mar 09 2015 Petr Hracek <phracek@redhat.com> - 2.0.1-5
- Unable to open file kickstart/untrackeduser (#1188280)
- preupg --upload requires slash after submit (#1195250)

* Fri Dec 12 2014 Jakub Dorňák <jdornak@redhat.com> - 2.0.1-4
- Update selinux related stuff
Resolves: #1150019

* Fri Nov 21 2014 Petr Hracek <phracek@redhat.com> - 2.0.1-3
- Wrong terminal output
- correct man page and help message
Resolves #1157443

* Thu Sep 04 2014 Petr Hracek <phracek@redhat.com> - 2.0.1-1
- Fix for correct return value in case of usage --riskcheck
Resolves: #1148878

* Thu Sep 04 2014 Petr Hracek <phracek@redhat.com> - 2.0.0-2
- preupgrade lists user home directory content in /var/cache/preupgrade
- provide descriptive error message (#1125228)
- Usability: suggest to install UI (#1125286)
Resolves: #1106498

* Tue Aug 05 2014 Petr Hracek <phracek@redhat.com> - 2.0.0-1
- Remove building openscap (#1113887)

* Wed Jul 30 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-37
- Preupgrade-assistant should be able to do cleanup of his files (#1123774)

* Tue Jul 29 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-36
- Don't verify db.sqlite and ui.log files

* Fri Jul 18 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-35
- Add support check_description
Resolves: #1078334

* Tue Jul 01 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-34
- Fix in case of calling preupg -c <contents>
- New package preupgrade-assistant-tools
- Removing some obsolete files
- Rename utils to preuputils dir
Resolves: #1113890

* Fri Jun 06 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-33
- rpm -V issue in preupgrade-assistant-ui package
Resolves: #1105482

* Thu Jun 05 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-32
Resolves: #1105120
- preupgrade-assistant doesn't check file updates

* Wed Jun 04 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-31
- Related: #1100192 Some fixes in HTML output

* Wed Jun 04 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-30
- Related: #1100192 Proper update of solution texts

* Thu May 29 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-29
- Add LICENSE text (#1102778)
- use rcue instead of patternfly rhbz#1102789

* Tue May 27 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-28
- Do not ship setuid binary (#1101698)
- Use correct preupg extension in config files

* Thu May 22 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-27
- Resolves: #1100192 Copy modified configuration files

* Wed May 21 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-26
- Fix for inplacerisk links

* Fri May 16 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-25
- Fix for running contents

* Thu May 15 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-24
- Support for repo metadata

* Wed May 14 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-23
- Fix for command log files traceback

* Mon May 12 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-22
- Fixes in solution text files
- Show only selected number of rules

* Mon May 05 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-21
- Support a 3rdpart contents

* Tue Apr 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0.2-20
- fix for unowned directories

* Fri Apr 25 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-19
- Fix: prefix issue #(1091074)

* Thu Apr 10 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-18
- Fixes in order to support premigrate command

* Mon Apr 07 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-17
- Fix API usage from redhat-upgrade-tool (#1084983)

* Thu Mar 27 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-16
- other fixes in setup.py

* Tue Mar 25 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-15
- Huge Python refactoring
- fix in setup.py

* Tue Mar 25 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-14
- Several Python fixes
- XML enhancement
- Supporting premigrate contents in /usr/share/premigrate

* Fri Mar 21 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-13
- Several fixes found during QA test days

* Wed Mar 19 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-12
- Text improvements
- debug option used for oscap output
- premigrate script for migration scenario

* Wed Mar 19 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-11
- Man page introduced
- Needs inspection in HTML page
- More enhancements in UI
- Improved CLI
- Several fixes

* Thu Mar 13 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-10
- Fix the bug #1075853

* Wed Mar 12 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-9
- new upstream version
- remove DEBUG info in case of no --debug option
- Introduced new states needs_action and needs_inspection
- several fixes

* Fri Mar 07 2014 Tomas Tomecek <ttomecek@redhat.com> - 1.0.2-8
- package UI: new package -ui
- cleanup

* Tue Feb 11 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-6
- Fix in common.sh
- Add user interactive messages

* Mon Feb 10 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-5
- Fix the problem with logger
- If all-xccdf.xml is not found in /usr/share/preupgrade then print message
- Remove all yaml directives

* Mon Feb 03 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-4
- New option riskcheck for preupgrade-assistant analysis
- Option will be mainly used by redhat-upgrade-tool

* Tue Jan 28 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-3
- Introduced special tags for solution texts

* Tue Jan 21 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-2
- adding postupgrade.d scripts
- logs are updated only in case of RPM database change

* Mon Jan 13 2014 Petr Hracek <phracek@redhat.com> - 1.0.2-1
- Update to the latest openscap version

* Fri Jan 10 2014 Petr Hracek <phracek@redhat.com> - 1.0.1-14
- Introduce remediation
- Some python fixes and improvements

* Tue Dec 17 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-13
- Using INI files instead of YAML
- Introduce tests in spec file
- Fix using preupg -c option

* Wed Dec 11 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-12
- Support more txt files in content directory
- Do not ship web-ui yet
- Bump preupgrade-assistant version

* Tue Dec 10 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-11
- Bump wrapper version because of API changes
- Functions have comments
- Reference to KICKSTART_README file
- /root/preupgrade/common directory is deleted
- dirtyconf and cleanconf directories are introduced

* Tue Dec 10 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-10
- Update description text

* Mon Dec 09 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-9
- Add README file
- more tests for OSCAP functionality

* Mon Dec 09 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-8
- Add progress bar during gathering common logs
- Copy some common files to kickstart

* Mon Dec 09 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-7
- python refactoring
- HTML escape fixes
- Add correct usage of <br/> tag

* Thu Dec 05 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-6
- post_script.txt file for post scan issues
- Introduce --scan option for scanning
- Introduce --list-contents for getting list of contents
- Introduce --check option for testing contents by maintainers

* Thu Dec 05 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-5
- Correct parsing rpm_qa.log in API

* Wed Dec 04 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-4
- solution text and solution script can be together a content

* Wed Dec 04 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-3
- description field does not used any HTML tag

* Wed Dec 04 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-2
- Introduce more tests
- Functions for postupgrade case
- Use <br> tag instead of <p> in HTML description

* Mon Dec 02 2013 Petr Hracek <phracek@redhat.com> - 1.0.1-1
- Bump to new upstream openscap release

* Mon Dec 02 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-18
- group_title is not needed anymore in YAML files
- Correct requirements in spec file
- with statement has not to be used in python code
- Add RHEL5 building issue
- correct XML generation in case of missing YAML values

* Thu Nov 28 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-17
- preupgrade-assistant-contents-users is not part of the spec file
- Hide some debug logs
- group_title is not needed anymore in YAML files
- All solution text will be replace automatically at the end

* Thu Nov 28 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-16
- All python sources are part of preupgrade-assistant package

* Wed Nov 27 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-15
- Solution text can be HTML
- Add license texts
- Execute preupgrade assistant without argument
- More contents

* Mon Nov 25 2013 Tomas Tomecek <ttomecek@redhat.com> - 0.9.11-14
- remove argparse dependency (use builtin optparse)

* Mon Nov 25 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-13
- Correct HTML syntax in solution text files

* Mon Nov 25 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-12
- SOLUTION_MSG has prefix which regards to content

* Sat Nov 23 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-11
- directory /var/cache/preupgrade/common is create automatically

* Fri Nov 22 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-10
- YAML is needed for the build

* Fri Nov 22 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-9
- New return value from content exit_failed (reference to exit_fail)

* Thu Nov 21 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-8
- YAML is not needed anymore
- New contents types
- Fixing BASH API

* Wed Nov 20 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-7
- Fix for current directory tag with relative path to all-xccdf.xml

* Tue Nov 19 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-6
- solution_file is introduced
- content dir is part of XML value.
- more generated functions like check_applies_to and check_rpm_to
- YAML is not needed for building RPMs anymore

* Wed Nov 06 2013 Pavel Raiskup <praiskup@redhat.com> 0.9.11-5
- rebuild to package recent upstream work

* Mon Nov 04 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-4
- common API is moved to preupgrade-assistant package

* Mon Nov 04 2013 Petr Hracek <phracek@redhat.com> - 0.9.11-4
- Fixed problems with wrong content

* Fri Nov 01 2013 Petr Hracek <phracek@redhat.com> 0.9.11-3
- Python refactoring and content definition

* Thu Oct 10 2013 Petr Hracek <phracek@redhat.com> 0.9.11-2
- temporary directory will be /var/tmp/preupgrade
- some corrections in create_group_xml.py and xccdf_compose.py files

* Thu Sep 12 2013 Petr Hracek <phracek@redhat.com> 0.9.11-1
- New openscap upstream version
- Supporting common modules

* Mon Dec 17 2012 Petr Hracek <phracek@redhat.com> 0.9.3-1
- Initial rpm
